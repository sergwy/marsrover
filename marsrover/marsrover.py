class Plateau:
    def __init__(self, params):
        size_x_s, size_y_s = params.split(' ')
        self.size_x = int(size_x_s)
        self.size_y = int(size_y_s)

    def __str__(self):
        return f'Max x size {self.size_x}. Max y size {self.size_y}'


class Rover:

    MOVE_DIRECTIONS = (1, 1, -1, -1)
    DIRECTIONS = ('N', 'E', 'S', 'W')
    __slots__ = ('pos_x', 'pos_y', 'direction', 'plateau')

    def __init__(self, plateau: Plateau, params: str):
        pos_x_s, pos_y_s, direction = params.split(' ')
        self.pos_x = int(pos_x_s)
        self.pos_y = int(pos_y_s)
        self.direction = self.DIRECTIONS.index(direction)
        self.plateau = plateau

    def turn_right(self):
        self.direction += 1
        if self.direction > len(self.MOVE_DIRECTIONS) - 1:
            self.direction = 0

    def turn_left(self):
        self.direction -= 1
        if self.direction < -len(self.MOVE_DIRECTIONS):
            self.direction = 3

    def move(self):
        if self.direction % 2 == 0:
            self.pos_y += self.MOVE_DIRECTIONS[self.direction]
            if self.pos_y < 0:
                self.pos_y = 0
            elif self.pos_y > self.plateau.size_y:
                self.pos_y = self.plateau.size_y
        else:
            self.pos_x += self.MOVE_DIRECTIONS[self.direction]
            if self.pos_x < 0:
                self.pos_x = 0
            elif self.pos_x > self.plateau.size_x:
                self.pos_x = self.plateau.size_x

    def process_commands(self, command_str: str):
        commands = {'R': self.turn_right, 'L': self.turn_left, 'M': self.move}
        for c in command_str:
            try:
                commands[c]()
            except KeyError:
                continue

        return str(self)

    def __str__(self):
        return f'{self.pos_x} {self.pos_y} {self.DIRECTIONS[self.direction]}'
