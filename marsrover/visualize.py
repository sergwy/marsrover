from sys import exit
import pygame
import re
from marsrover import Plateau, Rover


class RoverControlPanel:
    pygame.init()
    pygame.display.set_caption('ROVER CONTROL PANEL')

    GAME_FONT = pygame.font.match_font('monospace')
    FONT_LARGE = pygame.font.Font(GAME_FONT, 32)
    FONT_MEDIUM = pygame.font.Font(GAME_FONT, 18)
    FONT_SMALL = pygame.font.Font(GAME_FONT, 14)

    BLACK = pygame.Color('black')
    WHITE = pygame.Color('white')
    BLUE = pygame.Color('blue')
    GREEN = pygame.Color('green')
    RED = pygame.Color('red')

    COLOR_INACTIVE = GREEN
    COLOR_ACTIVE = WHITE

    height = 800
    width = 800
    screen = pygame.display.set_mode((width, height))
    clock = pygame.time.Clock()
    clock_tick = 50
    rover = None
    plateau = None
    color = COLOR_INACTIVE
    text = ''
    error_text = ''
    input_box_active = False
    input_box_rect = pygame.Rect(width / 3, 2, 200, 32)
    plateau_rect = None

    def __handle_event(self, event):
        if event.type == pygame.QUIT:
            exit()
        elif event.type == pygame.MOUSEBUTTONDOWN:
            self.input_box_active = self.input_box_rect.collidepoint(event.pos)
            self.color = self.COLOR_ACTIVE if self.input_box_active else self.COLOR_INACTIVE
        elif event.type == pygame.KEYDOWN:
            self.color = self.COLOR_ACTIVE
            if event.key in (pygame.K_RETURN, pygame.K_KP_ENTER):
                self.__action()
                self.text = ''
            elif event.key == pygame.K_BACKSPACE:
                self.text = self.text[:-1]
            elif re.match(r'\w| ', event.unicode) and len(self.text) <= self.input_box_rect.w // 10:
                self.text += event.unicode
            else:
                pass

    def __draw_input_box(self):
        txt_surface = self.FONT_SMALL.render(self.text, True, self.GREEN)
        self.screen.blit(txt_surface, (self.input_box_rect.x + 5, self.input_box_rect.y + 5))
        pygame.draw.rect(self.screen, self.color, self.input_box_rect, 2)

    def __draw_plateau(self):
        if self.plateau.size_x <= self.width - 300 \
                and self.plateau.size_y <= self.height - 300:
            pygame.draw.rect(self.screen, self.GREEN,
                             self.plateau_rect, 2)
        else:
            self.error_text = 'Plateau is too large'
            self.color = self.RED
            self.plateau = None

    def __get_rover_position(self):
        return (self.plateau_rect.x + self.rover.pos_x,
                self.plateau_rect.w + self.plateau_rect.y - self.rover.pos_y)

    def __draw_rover(self):
        pygame.draw.circle(self.screen, self.GREEN, self.__get_rover_position(), 3)

    def __show_text(self, font, text: str, color=None, dest=None):

        if color is None:
            color = self.GREEN

        if dest is None:
            dest = (self.width / 5.25, self.height / 2.5)

        resolve_text = font.render(text, True, color)
        self.screen.blit(resolve_text, dest=dest)

    def __action(self):
        if re.match(r'^\d+ \d+$', self.text):
            self.rover = None
            self.plateau = Plateau(self.text)
            self.plateau_rect = pygame.Rect(50, 100, self.plateau.size_x + 2, self.plateau.size_y + 2)
        elif re.match(r'^\d+ \d+ [NESWnesw]$', self.text) and self.plateau is not None:
            self.rover = Rover(self.plateau, self.text.upper())
        elif re.match(r'^[LlMmRr]+$', self.text) and self.rover is not None:
            self.rover.process_commands(self.text.upper())
        else:
            self.color = self.RED
            self.error_text = 'Error Processing Command'

    def run_control_panel(self):
        try:

            while True:
                for event in pygame.event.get():
                    try:
                        self.__handle_event(event)
                    except TypeError:
                        pass

                self.screen.fill(self.BLACK)
                self.__draw_input_box()

                if self.plateau is not None:
                    self.__draw_plateau()

                if self.rover is not None:
                    self.__draw_rover()
                    self.__show_text(self.FONT_SMALL, str(self.rover), color=self.WHITE,
                                     dest=self.__get_rover_position())

                if self.color == self.RED:
                    self.__show_text(self.FONT_LARGE, self.error_text, color=self.RED)

                pygame.display.update()
        except KeyboardInterrupt:
            exit()
