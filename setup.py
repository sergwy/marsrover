from setuptools import setup, find_packages

with open('LICENSE', 'rt', encoding='utf8') as lic, \
        open('requirements.txt', 'rt', encoding='utf8') as required_packages:
    setup(
        name='thoughtworks_mars_rover',
        maintainer='Sergey Yakimov',
        maintainer_email='sergwy@gmail.com',
        version='0.1.1',
        description='Thoughtworks Test Assignment',
        packages=find_packages(),
        install_requires=[req for req in required_packages],
        license=lic.read()
    )
