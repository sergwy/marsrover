import unittest
from marsrover import Rover, Plateau


class MarsRoverTestCase(unittest.TestCase):
    COMMAND_SET = ('5 5', '1 2 N', 'LMLMLMLMM', '3 3 E', 'MMRMMRMRRM')
    PLATEAU = Plateau(COMMAND_SET[0])

    def test_rover_one(self):
        rover_one = Rover(self.PLATEAU, self.COMMAND_SET[1])
        result_str = rover_one.process_commands(self.COMMAND_SET[2])
        self.assertEqual('1 3 N', result_str)

    def test_rover_two(self):
        rover_two = Rover(self.PLATEAU, self.COMMAND_SET[3])
        result_str_two = rover_two.process_commands(self.COMMAND_SET[4])
        self.assertEqual('5 1 E', result_str_two)


if __name__ == '__main__':
    unittest.main()
